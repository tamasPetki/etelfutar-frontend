import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {CategoriesComponent} from './categories/categories.component';
import {MenuComponent} from './menu/menu.component';
import {BasketComponent} from './basket/basket.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {OrderSummaryComponent} from './modals/order-summary/order-summary.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { LoginSignupComponent } from './modals/login-signup/login-signup.component';
import { NavbarComponent } from './navbar/navbar.component';
import {ReactiveFormsModule} from '@angular/forms';
import { SignupComponent } from './modals/login-signup/signup/signup.component';
import { LoginComponent } from './modals/login-signup/login/login.component';
import {TokenInterceptor} from './services/token-interceptor';
import { OrderSentComponent } from './modals/order-sent/order-sent.component';

@NgModule({
  declarations: [
    AppComponent,
    CategoriesComponent,
    MenuComponent,
    BasketComponent,
    OrderSummaryComponent,
    LoginSignupComponent,
    NavbarComponent,
    SignupComponent,
    LoginComponent,
    OrderSentComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgbModule,
    ReactiveFormsModule
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true}],
  entryComponents: [OrderSummaryComponent, LoginSignupComponent, OrderSentComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
}
