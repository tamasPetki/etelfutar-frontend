import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {OrderSentComponent} from '../order-sent/order-sent.component';
import {BasketService} from '../../services/basket.service';

@Component({
  selector: 'app-order-summary',
  templateUrl: './order-summary.component.html',
  styleUrls: ['./order-summary.component.css']
})
export class OrderSummaryComponent implements OnInit {
  @Input() basket;
  @Input() sum;

  constructor(public activeModal: NgbActiveModal, private modalService: NgbModal, private basketService: BasketService) {}

  ngOnInit(): void {
  }

  onOrderSent() {
    this.activeModal.close();
    this.modalService.open(OrderSentComponent);
  }

  emptyBasket() {
    this.basketService.emptyBasket();
    this.activeModal.close();
  }

}
