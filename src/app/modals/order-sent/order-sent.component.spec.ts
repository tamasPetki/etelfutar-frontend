import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderSentComponent } from './order-sent.component';
import {ReactiveFormsModule} from '@angular/forms';
import {NgbActiveModal, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';

describe('OrderSentComponent', () => {
  let component: OrderSentComponent;
  let fixture: ComponentFixture<OrderSentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderSentComponent ],
      imports: [ReactiveFormsModule, NgbModule, HttpClientModule],
      providers: [NgbActiveModal]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderSentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
