import { Component, OnInit } from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ServerService} from '../../services/server.service';
import {BasketService} from '../../services/basket.service';

@Component({
  selector: 'app-order-sent',
  templateUrl: './order-sent.component.html',
  styleUrls: ['./order-sent.component.css']
})
export class OrderSentComponent implements OnInit {

  shippingForm: FormGroup;

  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder, private server: ServerService, private basket: BasketService) { }

  ngOnInit() {
    this.shippingForm = this.fb.group({
      name: [null, Validators.required],
      address: [null, Validators.required],
      phone: [null]
    });

    this.server.orderSent.subscribe((value) => {
      console.log(value);
      if (value.status === 201) {
        this.activeModal.close();
        this.basket.emptyBasket();
      }
    });
  }


  sendOrder() {
    const order = this.shippingForm.value;
    order.uid = localStorage.getItem('userId');
    order.basket = this.basket.basket;
    this.server.onOrderSent(order);
  }

}
