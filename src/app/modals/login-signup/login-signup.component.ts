import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-loginandsignup',
  templateUrl: './login-signup.component.html',
  styleUrls: ['./login-signup.component.css']
})
export class LoginSignupComponent implements OnInit {

  register = false;

  constructor(public activeModal: NgbActiveModal, private auth: AuthService) {
  }

  ngOnInit() {
    this.auth.modalHandler.subscribe((value) => this.modalHandler(value));
  }

  modalHandler(event: {action: string, message?: string}) {
    const action = event.action;

    switch (action) {
      case 'register': {
        this.register = true;
        break;
      }
      case 'login': {
        this.register = false;
        break;
      }
      case 'close': {
        this.activeModal.close();
        break;
      }
    }
  }

}
