import {AfterContentInit, AfterViewInit, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ServerService} from '../../../services/server.service';
import {AuthService} from '../../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  errorMessage: string;

  constructor(private auth: AuthService, public fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      password: ['', Validators.required],
      email: ['', Validators.required]
    });
    this.auth.modalHandler.subscribe(value => {
      if(value.action === 'errormessage') {
        this.errorMessage = value.message;
      }
    });
  }

  loginUser() {
    this.auth.login(this.loginForm.value);
  }

  changeModal() {
    this.auth.registrationModal();
  }

}
