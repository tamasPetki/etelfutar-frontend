import { Component, OnInit } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {LoginSignupComponent} from '../modals/login-signup/login-signup.component';
import {AuthService} from '../services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private modalService: NgbModal, private auth: AuthService) { }

  ngOnInit() {
  }

  openLogin() {
    const modalRef = this.modalService.open(LoginSignupComponent);
  }

  isLoggedIn() {
    return this.auth.isAuthenticated();
  }

  logOut() {
    this.auth.logout();
  }

}
