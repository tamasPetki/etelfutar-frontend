import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ServerService} from '../services/server.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {



  constructor(private server: ServerService) { }

  ngOnInit() {
    this.server.getCategories();
  }

  getCategories() {
    return this.server.categories;
  }

  onCategoryClicked(category: string) {
    this.server.onCategoryChosen(category);
  }
}
