import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {isUndefined} from 'util';
import {Subject, Subscription} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ServerService {

  menuItems: any;
  categories: any;
  categorySubscription: Subscription;
  menuItemSubscription: Subscription;
  orderSent = new Subject<any>();

  constructor(private http: HttpClient) {
  }

  getMenuItemsObservable(category?: string) {
    if (isUndefined(category)) {
      // DEFAULT
      return this.http.get('https://etelfutar.herokuapp.com/menuitems');
    } else {
      // CHOSEN CATEGORY
      console.log('chosen: ' + category);
      return this.http.get('https://etelfutar.herokuapp.com/category/' + category);
    }
  }

  getCategoriesObservable() {
    return this.http.get('https://etelfutar.herokuapp.com/categories');
  }

  getCategories() {
    this.categorySubscription = this.getCategoriesObservable().subscribe((values) => this.categories = values, error => console.log(error));
  }

  getMenuItems() {
    this.menuItemSubscription = this.getMenuItemsObservable().subscribe((values) => this.menuItems = values, error => console.log(error));
  }

  onCategoryChosen(category: string) {
    this.menuItemSubscription.unsubscribe();
    this.menuItemSubscription = this.getMenuItemsObservable(category).subscribe((values) => this.menuItems = values, error => console.log(error));
  }

  onOrderSent(order) {
    this.http.post('https://etelfutar.herokuapp.com/sendorder', order, { observe: 'response' }).subscribe((respond) => {
      this.orderSent.next(respond);
      console.log(respond);
      },
        error => console.log(error));
  }
}
