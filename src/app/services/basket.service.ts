import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BasketService {
  basket = [];
  errorMessage = '';

  constructor() {
  }

  addToBasket(item) {
    // DELIBERATE BUG
    // if ((this.calculateSum() > 10000) && (this.basket.length > 0)) {
    //   throw new Error('Bug');
    // }


    if ((item.Price + this.calculateSum()) > environment.maxBasket) {
      this.errorMessage = `Sorry, you can\'t add more than ${environment.maxBasket} into your basket.`;
      setTimeout(() => this.errorMessage = '', 5000);
      return null;
    }
    if (this.basket.includes(item)) {
      const resultIndex = this.basket.indexOf(item);
      return this.basket[resultIndex].Qty += 1;
    }
    item.Qty = 1;
    this.basket.push(item);
  }

  removeFromBasket(item) {
    if (this.basket.includes(item)) {
      const resultIndex = this.basket.indexOf(item);
      if (this.basket[resultIndex].Qty > 1) {
        return this.basket[resultIndex].Qty -= 1;
      } else {
        return this.basket.splice(resultIndex, 1);
      }
    } else {
      return null;
    }
  }

  calculateSum() {
    if (this.basket == null) {
      return 0;
    }
    return this.basket.reduce((a, b) => {
      return b['Price'] == null ? a : a + (b['Price'] * b['Qty']);
    }, 0);
  }

  emptyBasket() {
    this.basket = [];
  }
}
