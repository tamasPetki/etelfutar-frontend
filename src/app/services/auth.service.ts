import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {tokenNotExpired} from 'angular2-jwt';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  modalHandler = new Subject<{ action: string, message?: string }>();

  constructor(private http: HttpClient) {
  }

  signUp(signUpFrom) {
    return this.http.put('https://etelfutar.herokuapp.com/signup', signUpFrom, {observe: 'response'}).subscribe(resp => {
      this.modalHandler.next({action: 'close'});
    }, error => {
      console.log('Something went wrong: ', error.error.errors);
      this.errorMessage(error.error.errors[0].msg);
    });
  }

  login(loginForm) {
    return this.http.post('https://etelfutar.herokuapp.com/login', loginForm).subscribe(res => {
        if (res['token'] && res['userId']) {
          console.log('Successfully logged in');
          localStorage.setItem('token', res['token']);
          localStorage.setItem('userId', res['userId']);
          this.closeModal();
        } else {
          console.log(res);
          this.errorMessage('Invalid username or password');
        }
      },
      error1 => console.log(error1));
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
  }

  getToken() {
    return localStorage.getItem('token');
  }

  public isAuthenticated() {
    const token = this.getToken();
    return tokenNotExpired(null, token);
  }

  closeModal() {
    this.modalHandler.next({action: 'close'});
  }

  registrationModal() {
    this.modalHandler.next({action: 'register'});
  }

  loginModal() {
    this.modalHandler.next({action: 'login'});
  }

  errorMessage(message: string) {
    this.modalHandler.next({action: 'errormessage', message: message});
  }

}
