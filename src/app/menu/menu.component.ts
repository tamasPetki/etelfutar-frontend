import { Component, OnInit } from '@angular/core';
import {ServerService} from '../services/server.service';
import {BasketService} from '../services/basket.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {


  constructor(private server: ServerService, private basket: BasketService) {
  }

  ngOnInit() {
    this.server.getMenuItems();
  }

  getMenuItems() {
    return this.server.menuItems;
  }

  addToBasket(item) {
    this.basket.addToBasket(item);
  }
}
