import { Component, OnInit } from '@angular/core';
import {ServerService} from '../services/server.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {OrderSummaryComponent} from '../modals/order-summary/order-summary.component';
import {AuthService} from '../services/auth.service';
import {LoginSignupComponent} from '../modals/login-signup/login-signup.component';
import {BasketService} from '../services/basket.service';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css']
})
export class BasketComponent implements OnInit {

  constructor(private auth: AuthService, private server: ServerService, private modalService: NgbModal, private basket: BasketService) { }

  ngOnInit() {
  }

  getBasket() {
    return this.basket.basket;
  }

  getSum() {
    return this.basket.calculateSum();
  }

  removeItem(item) {
    this.basket.removeFromBasket(item);
  }

  addItem(item) {
    this.basket.addToBasket(item);
  }

  getErrormessage() {
    return this.basket.errorMessage;
  }

  onOrder() {
    if (this.auth.isAuthenticated()) {
      const modalRef = this.modalService.open(OrderSummaryComponent);
      modalRef.componentInstance.basket = this.basket.basket;
      modalRef.componentInstance.sum = this.basket.calculateSum();
    } else {
      this.modalService.open(LoginSignupComponent);
    }
  }

}
