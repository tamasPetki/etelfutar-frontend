import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BasketComponent} from './basket.component';
import {HttpClientModule} from '@angular/common/http';
import {BasketService} from '../services/basket.service';

describe('BasketComponent', () => {
  let component: BasketComponent;
  let fixture: ComponentFixture<BasketComponent>;
  const testItem = {
    id: 1,
    Category: 'Starter',
    Description: 'Saláta csirkemellel, uborkával, pirított kenyérkockával',
    Name: 'Cézár saláta',
    Price: 5700,
    Spicy: 0,
    Vegatarian: 0
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BasketComponent],
      imports: [
        HttpClientModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add item to basket', () => {
    const basketService = fixture.debugElement.injector.get(BasketService);
    fixture.detectChanges();
    basketService.addToBasket(testItem);
    expect(basketService.basket[0]).toBe(testItem);
  });

  it('should throw an error when the basket is not empty and gets over 10000', () => {
    const basketService = fixture.debugElement.injector.get(BasketService);
    fixture.detectChanges();
    basketService.addToBasket(testItem);
    basketService.addToBasket(testItem);
    expect(() => basketService.addToBasket(testItem)).toThrow(new Error('Bug'));
  });
});
